/**
 * App routes
 */
var app = (function(){
    'use strict';

    // region Deps

    var
        angular = require('angular'),
        httpProviderConfig = require('./../config/http-provider-config.js'),
        routes = require('../config/routes.js'),
        routeCallbackConfig = require('../config/routeCallbacks.js'),
        controllers = require('./controllers.js'),
        services = require('./services.js'),
        values = require('./values.js'),
        constants = require('./constants.js'),
        filters = require('./filters.js'),
        models = require('./models.js'),
        modelSet = require('../data/models/modelSet.js'),
        directives = require('./directives.js');

    // endregion

    // region Private Fields

    var mainModule = angular.module('app.core', [
            'ui.router',
            'breeze.angular',
            'ngAnimate',
            'app.services',
            'app.controllers',
            'app.filters',
            'app.models',
            'app.directives'
        ]),
        servicesModule = angular.module('app.services', []),
        controllersModule = angular.module('app.controllers', []),
        filtersModule = angular.module('app.filters', []),
        modelsModule = angular.module('app.models', []),
        directivesModule = angular.module('app.directives', []);

    // endregion

    // region Register all Controllers

    controllers.forEach(function(controller){
        controllersModule.controller(controller.name, controller.ctrl);
    });

    // endregion

    // region Register All Services

    services.concat(values).concat(constants).forEach(function(service){
        servicesModule[service.type](service.name, service.service);
    });

    // endregion

    // region Register All Filters

    filters.forEach(function(service){
        filtersModule[service.type](service.name, service.service);
    });

    // endregion

    // region Register All Models

    models.forEach(function(model){
        modelsModule.factory(model.name, model.ctor);
    });

    modelsModule.value(modelSet);

    // endregion

    // region Register All Directives

    directives.forEach(function(directive){
        directivesModule.directive(directive.name, directive.directive);
    });

    // endregion

    // region Config Phase

    httpProviderConfig.configure(mainModule);
    routes.configure(mainModule);
    routeCallbackConfig.configure(mainModule);

    servicesModule.config(['notifierProvider', function(notifierProvider){
        notifierProvider.init({ debugMode: true });
    }]);

    // endregion

    // region Run Phase


    // endregion

    // region Public API

    return mainModule;

    // endregion

})();

// region CommonJS

module.exports = app;

// endregion