/**
 * A directive that triggers the breeze initialization process, this is used becuase we want the rootScope to be populated with the metadata before the init process starts
 */
 (function(){
    'use strict';

    var breezeInitDirective= function breezeInitDirective(breezeService){

        // region Inner Methods

        var
            linkFn = function linkFn(scope, element){
                breezeService.init();
                element.remove();
            };

        // endregion

        return {
            restrict: 'AE',
            link: linkFn
        };
    };

    // region CommonJS

    module.exports = {
        name: 'breezeInit',
        directive: ['breezeService', breezeInitDirective]
    };

    // endregion

 })();