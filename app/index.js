'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var yosay = require('yosay');

var YoWebUiGenerator = yeoman.generators.Base.extend({
	promptUser: function() {
        var done = this.async();
 
        // have Yeoman greet the user
        console.log(this.yeoman);
 
        var prompts = [{
            name: 'appName',
            message: 'What is your app\'s name ?'
        }];
 
        this.prompt(prompts, function (props) {
            this.appName = props.appName;
   
            done();
        }.bind(this));
    },

    scaffoldFolders: function(){
    	this.mkdir("auth");
    	this.mkdir("bin");
    	this.mkdir("config");
    	this.mkdir("grunt");
    	this.mkdir("public");

    	this.mkdir("public/content");
    	this.mkdir("public/content/css");
    	this.mkdir("public/content/css/app");
    	this.mkdir("public/content/css/app/settings");
    	this.mkdir("public/content/css/font-awesome");
    	this.mkdir("public/content/css/font-awesome/css");
    	this.mkdir("public/content/css/foundation");
    	this.mkdir("public/content/css/foundation/foundation");
    	this.mkdir("public/content/css/foundation/foundation/components");   	
    	this.mkdir("public/content/images");

    	this.mkdir("public/js");
    	this.mkdir("public/js/app");
    	this.mkdir("public/js/app/auth");
    	this.mkdir("public/js/app/config");
    	this.mkdir("public/js/app/core");
    	this.mkdir("public/js/app/data");
    	this.mkdir("public/js/app/data-services");
    	this.mkdir("public/js/app/home");
    	this.mkdir("public/js/app/partial");
    	this.mkdir("public/js/app/shared");
    	this.mkdir("public/js/app/tmp");
    	this.mkdir("public/js/app/utils");

    	this.mkdir("public/js/tests");
    	this.mkdir("public/js/tests/e2e");
    	this.mkdir("public/js/tests/unitTests");
    	this.mkdir("public/js/tests/unitTests/config");

    	this.mkdir("routes");
    	this.mkdir("utils");
    	this.mkdir("views");
    },

    copyFiles: function() {
	    this.directory("", "");

	    this.template("package.json", "package.json", { app_name: this.appName });
	}

	/*runNpm: function(){
		var done = this.async();

		this.npmInstall("", function(){
			console.log("\nEverything Setup !!!\n");
			done
		});
	}*/
});

module.exports = YoWebUiGenerator;
