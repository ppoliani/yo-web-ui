/**
 * Loads all the controllers of the app
 */
module.exports = [
    require('../partial/header/headerCtrl.js'),
    require('../home/homeCtrl.js'),
    require('../auth/authCtrl.js'),
    require('../tmp/peopleCtrl.js')
];