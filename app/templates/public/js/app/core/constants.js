/**
 * Contains all the constants
 */
module.exports = [{
    name: 'SERVICE_ENDPOINT',
    type: 'constant',
    service: 'http://localhost:9999/api/v1/'
}];