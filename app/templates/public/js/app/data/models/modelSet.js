/**
 * Aggregates all the models as angular dependencies
 */
(function(){
    'use strict';

    var modelSet = function(Person){
        return {
            Person: Person
        };
    };

    // region CommonJS

    module.exports = {
        name: 'modelSet',
        type: 'factory',
        service: [
            'Person',
            modelSet
        ]
    };

    // endregion
})();