/**
 * A collection of all the repositories in the application
 */
(function () {
    'use strict';

    var RepositoriesService = function RepositoriesService(RepositoryService) {

        // region Inner Fields

        var Repository = RepositoryService.Repository;

        // endregion

        // region Public API

        return {
            personRepository: new Repository('Person', 'People')
        };

        // endregion
    };

    // region CommonJS

    module.exports = {
        name: 'RepositoriesService',
        type: 'factory',
        service: ['RepositoryService', RepositoriesService]
    };

    // endregion

})();