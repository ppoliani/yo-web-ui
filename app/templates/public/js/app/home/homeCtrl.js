/**
 * Home Controller
 */
(function(){
    'use strict';

    // region Controller

    var homeCtrl = function($scope){
        $scope.title = 'Hello Main Controller';
    };

    // endregion

    // region CommonJS

    module.exports = {
        name: 'homeCtrl',
        ctrl: ['$scope', homeCtrl]
    };

    // endregion
})();

