/**
 * App entry point
 */
var httpProviderConfig = (function(){
    'use strict';

    // region Consts

    // This is relative to the nodeJS static files directory !!!
    var BASE_DIR = '/js/app/'

    // endregion

    // region Inner Methods

    var
        /**
         * Runs the configurations
         */
        configure = function(app){
            app.config(['$stateProvider',
                '$urlRouterProvider',
                '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){
                    $stateProvider
                        .state('home', {
                            url: '/',
                            templateUrl: _getPath('home/index'),
                            controller: 'homeCtrl'
                        })

                        .state('login', {
                            url: '/login',
                            templateUrl: _getPath('auth/login'),
                            controller: 'authCtrl'
                        })

                        .state('people', {
                            url: '/people',
                            templateUrl: _getPath('tmp/people'),
                            controller: 'peopleCtrl',
                            claims: {
                                role: ['admin']
                            }
                        })
                    ;

                    $urlRouterProvider.otherwise('/');
                    $locationProvider.html5Mode(true);
                }]);
        },

        /**
         * Return the paths relative to base dir
         *
         * @param filePath
         * @returns {string}
         * @private
         */
        _getPath = function(filePath){
            return BASE_DIR + filePath + '.html';
        };

    // endregion

    // region Public API

    return {
        configure: configure
    };

    // endregion
})();

// region Exports

module.exports = httpProviderConfig;

// endregion