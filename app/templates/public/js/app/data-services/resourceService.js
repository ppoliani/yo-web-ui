/**
 * service that executes xhr requests to the back-end
 */
(function(){
    'use strict';

    var resourceService = function resourceService($resource){

        // region Consts

        var API_URL = 'http://localhost:9090/api/v1/';

        // endregion

        // region Inner Methods

        var
            /**
             * Executes an http GET request
             * @param url
             */
            get = function get(url){
                throw 'Not Implemented';
            },

            /**
             * Executes an http POST request
             * @param url
             * @param data
             */
            post = function post(url, data){
                throw 'Not Implemented';
            },

            /**
             * Executes an http PUT request
             * @param url
             * @param data
             */
            put = function put(url, data){
                throw 'Not Implemented';
            },

            /**
             * Gets the path for the given resource
             * @param resouce
             * @returns {string}
             * @private
             */
            _getPath = function _getPath(resource){
                return API_URL + resouce;
            };

        // endregion

        // region public API

        return {
            get: get,
            post: post,
            put: put,
            apiUrl: API_URL
        };

        // endregion
    };

    // region CommonJS

    module.exports = {
        name: 'resourceService',
        type: 'factory',
        service: ['$resource', resourceService]
    };

    // endregion
})();

