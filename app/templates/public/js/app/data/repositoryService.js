/**
 * The repository service
 */
(function () {
    'use strict';

    var RepositoryService = function RepositoryService(breeze, notifier, RepositoryQueryService, breezeService) {

        // region Inner Fields

        var entityManager = breezeService.entityManager;

        // endregion

        // region Inner Methods

        var
            /**
             * Returns the entity with the given id
             * @param id
             */
            findById = function findById(id){
                return entityManager.getEntityByKey(this.entityName, id);
            },

            /**
             * Inserts the given entity into the cache
             * @param entity
             */
            attach = function attach(entity){
                return entityManager.createEntity(this.entityName, entity);
            },

            /**
             * Updates the given entity
             * @param entity
             */
            update = function update(entity){
                // This will overwirte the entity in the cache with the same id
                return entityManager.createEntity(this.entityName, entity, null, breeze.MergeStrategy.PreserveChanges);
            },

            /**
             * Deletes the given entity
             * @param entity
             */
            deleteEntity = function deleteEntity(entity){
                entity.entityAspect.setDeleted();
            },

            /**
             * Return a new instance of the repository query class
             */
            query = function query(){
                return new RepositoryQueryService.RepositoryQuery(this);
            },

            /**
             * Will execute and return the query with the given options
             * @param opts
             */
            get = function get(opts){
                var q = breeze.EntityQuery
                    .from(this.resourceName);

                if(opts.queryLocally){
                    q = q.toType(this.entityName);
                }

                if(opts.filter){
                    q = q.where(breeze.Predicate.or(opts.filter));
                }

                if(opts.id){
                    q = q.findById(opts.id);
                }

                if(opts.orderBy){
                    q = q.orderBy(opts.orderBy);
                }

                if(opts.includePropeties){
                    q = q.expand(opts.includePropeties);
                }

                if(opts.skip){
                    q = q.skip(opts.skip)
                }

                if(opts.take){
                    q = q.take(opts.take);
                }

                if(opts.queryLocally){
                    return entityManager.executeQueryLocally(q);
                }

                else {
                    var promise = entityManager.executeQuery(q);

                     promise
                        .then(_querySucceeded.bind(this))
                        .catch(_queryFailed.bind(this));

                    return promise;
                }
            },

            /**
             * Success callback
             * @private
             */
            _querySucceeded = function _querySucceeded(){
                notifier.success('Data for [' + this.entityName + '] was loaded');
            },

            /**
             * Fail callback
             * @param error
             * @private
             */
            _queryFailed = function _queryFailed(error){
                notifier.error('Error retrieving data for the entity type [' + this.entityName + '] ' + error.message);
            };

        // endregion

        // region Ctor

        var Repository = function(entityName, resourceName){
            this.entityName = entityName;
            this.resourceName = resourceName;
        };

        Repository.prototype = (function(){

            // region Public Api

            var publicApi = {
                constructor: Repository,
                findById: findById,
                attach: attach,
                update: update,
                deleteEntity: deleteEntity,
                query: query,
                get: get
            };

            // endregion

            return publicApi;
        })();

        // endregion

        // region Public API

        return {
            Repository: Repository
        };

        // endregion
    };

    // region CommonJS

    module.exports = {
        name: 'RepositoryService',
        type: 'factory',
        service: [
            'breeze',
            'notifier',
            'RepositoryQueryService',
            'breezeService',
            RepositoryService
        ]
    };

    // endregion

})();