/**
 * Loads all the services of the app
 */
module.exports = [
    require('../auth/authService.js'),
    require('../auth/routeAuthService.js'),
    require('../data-services/resourceService.js'),
    require('../utils/notifierProvider.js'),
    require('../utils/pubsub.js'),

    // Data
    require('../data/breezeService.js'),
    require('../data/jsonResultsAdapterService.js'),
    require('../data/models/modelSet.js'),
    require('../data/repositoryQueryService.js'),
    require('../data/repositoryService.js'),
    require('../data/repositoriesService.js'),
    require('../data/unitOfWorkService.js')
];