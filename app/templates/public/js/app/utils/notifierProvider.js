/**
 * A notifier service
 */
(function () {
    'use strict';

    var notifierProvider = function notifierProvider() {

        // region Inner Fields

        var debugMode = false;

        // endregion

        // region Inner Methods

        var
            /**
             * Initializes the service
             * @param opts
             */
            init = function(opts){
                debugMode = opts.debugMode;
            };

        // endregion

        // region Public API

        return {
            // notifierService
            $get: ['$q', 'alertify', function($q, alertify){
                var
                    /**
                     * Alerts an info message
                     * @param message
                     */
                    info = function(message){
                        if(debugMode) {
                            alertify.log(message);
                        }
                    },

                    /**
                     * Alerts the given succes message
                     * @param message
                     */
                    success = function(message){
                        if(debugMode) {
                            alertify.success(message);
                        }
                    },

                    /**
                     * Alerts an error message
                     * @param message
                     */
                        error = function(message){
                            if(debugMode) {
                                alertify.log(message);
                            }
                        },

                    /**
                     * Show a dialog
                     */
                    confirm = function(message){
                        if(debugMode){
                            var deferred = $q.defer();

                            alertify.confirm(message, function(e){
                                if(e){
                                    deferred.resolve(true);
                                }
                                else {
                                    deferred.resolve(false);
                                }
                            });

                            return deferred.promise;
                        }
                    };

                return {
                    info: info,
                    success: success,
                    error: error,
                    confirm: confirm
                }
            }],
            init: init
        };

        // endregion
    };

    // region CommonJS

    module.exports = {
        name: 'notifier',
        type: 'provider',
        service: [notifierProvider]
    };

    // endregion

})();