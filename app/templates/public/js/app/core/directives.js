/**
 * Loads all the directives
 */
module.exports = [
    require('../shared/preloadResourceDirective.js'),
    require('../shared/breezeInitDirective.js')
];