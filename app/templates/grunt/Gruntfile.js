﻿
module.exports = function (grunt) {
    //Without matchdep, we would have to write grunt.loadNpmTasks("grunt-task-name"); for each dependency
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
    
    var config = require('./config'),
        webpack = require('webpack');

    grunt.initConfig({
        distDir: '../public/dist',

        libsDir: '../public/js/third-party',

        jsDir: '../public/js',

        cssDir: '../public/content/css',

        appDir: '../public/js/app',

        testDir: '../public/js/tests/**/*.*',

        // read the package.json file
        pkg: grunt.file.readJSON('package.json'),

        /********************* Connect ***********************/
        express: {
            dev: {
                options: {
                    script: '../app.js',
                    livereload: true
                }
            }
        },

        /********************* Clean ***********************/
        clean: {
            options: {
                force: true
            },
            dist: ['<%= distDir %>/*', '!<%= distDir %>/sass/**', '!<%= distDir %>/fonts/**'],
            sass: ['<%= distDir %>/sass/**']
        },

        /********************* Copy files ***********************/
        copy: {
            dev: {
                files: [{
                        expand: true,
                        src: config.copyFiles.fontAwesomeFonts,
                        dest: '<%= distDir %>/fonts/',
                        flatten: true,
                        filter: 'isFile'
                    }]
            }
        },

        /********************* Webpack ***********************/
        webpack: {
            dev: {
                entry: "<%= appDir %>/core/app.js",
                output: {
                    path: '<%= distDir %>',
                    filename: 'bundle.js'
                },

                externals: {
                    'angular': 'angular'
                },

                devtool: '#source-map'
                /*plugins: [
                    new webpack.ProvidePlugin({
                        angular: '../third-party/angular/angular.js'
                    })
                ]*/
            }
        },

        /********************* Strip-code  ***********************/
        strip_code: {
            options: {
                start_comment: 'start-test-block',
                end_comment: 'end-test-block'
            },
            src: 'dist/*.js'
        },

        /********************* Concat  (Depricated! Webpack used instead. Concats third party scripts only)***********************/
        concat: {
            options: {
                stripBanners: true,
                separator: ';'
            },

            vendor: {
                src: config.bundles.js.vendor,
                dest: '<%= distDir %>/vendor.js'
            }
        },

        /********************* Uglify ***********************/
        uglify: {
            options: {
                    banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n' +
                            '* Copyright (c) <%= grunt.template.today("yyyy") %> Brandview - Pavlos Polianidis\n' +
                            '*/\n'
            },

            dev: {
                src: ['../public/js/main.js'],
                dest: '../public/js/dev/brandview.ganttchart.min.js'
            }
        },

        /********************* Karma ***********************/
        karma: {
            dev: {
                configFile: 'karma.conf.js'
            }
        },

        /********************* Protractor ***********************/
        protractor: {
            options: {
                configFile: 'protractor.conf.js', // Default config file
                keepAlive: true, // If false, the grunt process stops when the test fails.
                noColor: false, // If true, protractor will not use colors in its output.
                args: {
                    // Arguments passed to the command
                }
            },
            dev: {
                options: {
                    configFile: "protractor.conf.js", // Target-specific config file
                    args: {} // Target-specific arguments
                }
            },
        },

        /********************* Html min ***********************/
        htmlmin: {
            dev: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                }
            }
        },

        /********************* SASS *********************/
        sass: {
            options: {
                style: 'expanded',
                banner: config.banner,
                compass: true,
                force: true
            },

            app: {
                files: config.bundles.sass.app
            },

            vendor: {
                files: config.bundles.sass.vendor
            },

            dist: {
                options: {
                    style: 'compressed',
                    compass: true
                },
                files: config.bundles.sass
            }
        },

        /********************* Css min ***********************/
        cssmin: {
            vendor: {
                src: config.bundles.css.vendor,
                dest: '<%=distDir%>/css/vendor.min.css'
            }
        },

        /********************* jsHint ***********************/
        jshint: {
            build: ['../public/js/*.*']
        },

        /********************* Watch ***********************/
        watch: {
            options: {
                livereload: true,
                nospawn: true
            },

            dev: {
                files: ['<%= appDir %>/**/*.js', '<%= cssDir %>/app/**/*.scss'],
                tasks: ['watchTasks']
            }
        },

        /********************* Uncss ***********************/
        uncss: {
            dev: {
                files: [{
                    src: '*.aspx',
                    dest: 'css/build/styles.css'
                }],

                options: {
                    htmlroot: '../grocer'
                }
            }
        },

        /********************* Jsdoc ***********************/
        jsdoc: {
            dist: {
                src: ['<%= jsDir %>/**/*.js'],
                options: {
                    destination: '<%= distDir %>/docs'
                }
            }
        },

        /********************* plaro ***********************/
        plato: {
            dev: {
                src: ['../public/js/**/*.js', '!../public/js/dev/**/*.*', '!<%= testDir %>'],
                dest: '<%= distDir %>/codeAnalysis'
            }
        }
    });

    grunt.registerTask('test', ['clean',  'karma:dev']);
    grunt.registerTask('build', ['clean:dist', 'webpack:dev', 'concat:vendor', 'sass:app', 'sass:vendor', 'cssmin:vendor', 'copy:dev']);
    grunt.registerTask('dev', ['clean:dist', 'build', 'watch:dev']);
    grunt.registerTask('watchTasks', ['webpack:dev', 'sass:app']);
    grunt.registerTask('buildCss', ['clean', 'sass:dev', 'cssmin:vendor', 'clean:sass']);
};