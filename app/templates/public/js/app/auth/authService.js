/**
 * The service that deals with auth-related stuff
 */
(function() {
    'use strict';

    // region Service

    var authService = function authService($http, $window, $location, notifier){

        // region Inner Methods

        var
            /***
             * Checks if user is logged-in; in which case redirect to the previous url
             * @private
             */
            checkIfLoggedIn = function checkIfLoggedIn(){
                notifier.info('Checking if use logged-in');

                var accessToken = $window.sessionStorage.access_token;

                if(accessToken){
                    $location.path('/');
                }
            },

            /**
             * Will try to login with the given credentials
             */
            login = function login(username, password){
                var data = 'grant_type=password&username=' + username + '&password=' + password + '&scope=email',
                    accessToken = $window.sessionStorage.access_token;

                $http({
                    method: 'POST',
                    url: 'http://localhost:9090/oauth/token/',
                    data: data,
                    headers: {
                        Authorization: 'Basic YWJjMTIzOnNzaHNlY3JldA==',
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .success(function(data){
                    console.log(data);

                    $window.sessionStorage.access_token = data['access_token'];
                    $window.sessionStorage.user_info = JSON.stringify(data['user_info']);
                })
                .error(function(err){
                    console.log(err);
                });
            },

            /**
             * Returns the info for the loged in user
             */
            getUserInfo = function getUserInfo(){
                return $window.sessionStorage.user_info
                    && JSON.parse($window.sessionStorage.user_info);
            };

        // endregion

        return {
            login: login,
            checkIfLoggedIn: checkIfLoggedIn,
            getUserInfo: getUserInfo
        };
    };

    // endregion

    // region CommonJS

    module.exports = {
        name: 'authService',
        type: 'factory',
        service: ['$http', '$window', '$location', 'notifier', authService]
    };

    // endregion
})();