/**
 * Authentication controller
 */
(function(){
    'use strict';

    var authCtrl = function authCtrl($scope, authService){

        // region Inner Methods

        var
            /**
             * Will call the login method of the auth service
             */
            login = function login(){
                authService.login($scope.username, $scope.password);
            };

        // endregion

        authService.checkIfLoggedIn();

        // region Viewmodel

        $scope.username = '';
        $scope.password = '';

        $scope.login = login;

        // endregion
    };

    module.exports = {
        name: 'authCtrl',
        ctrl: ['$scope', 'authService', authCtrl]
    };
})();