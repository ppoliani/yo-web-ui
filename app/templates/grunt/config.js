﻿
var bundles = {
    js: {
        vendor: [
            '<%= libsDir %>/angular/angular.js',
            '<%= libsDir %>/angular-animate/angular-animate.js',
            '<%= libsDir %>/angular-ui-router/release/angular-ui-router.js',
            '<%= libsDir %>/breezejs/breeze.debug.js',
            '<%= libsDir %>/breezejs/labs/breeze.angular.js',
            '<%= libsDir %>/alertify.js/lib/alertify.js'
        ],

        app: [
            '<%= appDir %>/config/http-provider-config.js',
            '<%= appDir %>/config/routes.js',
            '<%= appDir %>/app.js'
        ]
    },

    css: {
        vendor: [
            '<%= distDir %>/sass/foundation.css',
            '<%= distDir %>/sass/foundation.normalize.css',
            '<%= libsDir %>/alertify.js/themes/alertify.core.css',
            '<%= libsDir %>/alertify.js/themes/alertify.default.css',
            '<%= cssDir %>/font-awesome/css/font-awesome.css'
        ],

        app: [
            '<%= distDir %>/sass/app.css'
        ]
    },

    sass: {
        vendor: {
            '<%= distDir %>/sass/foundation.css': '<%= cssDir %>/foundation/foundation.scss',
            '<%= distDir %>/sass/foundation.normalize.css': '<%= cssDir %>/foundation/normalize.scss'
        },

        app: {
            '<%= distDir %>/app.css': '<%= cssDir %>/app/app.scss'
        }
    }
};

bundles.testScripts = [
    '<%= libsDir %>/angular/angular.min.js',
    '<%= libsDir %>/angular/angular-ui-router/release/angular-ui-router.min.js',
    '<%= jsDir %>/breezejs/breeze.debug.js'
].concat(bundles.js);

var copyFiles = {
    fontAwesomeFonts: '<%= cssDir %>/font-awesome/fonts/**'
};

var banner = '/*!\n' +
    ' * <%= pkg.name %>\n' +
    ' * <%= pkg.title %>\n' +
    ' * <%= pkg.url %>\n' +
    ' * @author <%= pkg.author %>\n' +
    ' * @version <%= pkg.version %>\n' +
    ' * Copyright <%= pkg.copyright %>. <%= pkg.license %> licensed.\n' +
    ' */\n';

module.exports.bundles = bundles;
module.exports.banner = banner;
module.exports.copyFiles = copyFiles;