/**
 * Header ctrl
 */
(function () {
    'use strict';

    var headerCtrl = function headerCtrl($scope) {
        // region Inner fields

        // endregion

        // region Inner Methods

        // endregion

        // region Viewmodel

        $scope.isMenuVisible = false;

        /**
         * Toggles the visibility of the menu
         */
        $scope.toggleMenu = function toggleMenu(){
            $scope.isMenuVisible = !$scope.isMenuVisible;
        };

        // endregion
    };

    // region CommonJS

    module.exports = {
        name: 'headerCtrl',
        ctrl: ['$scope', headerCtrl]
    };

    // endregion

})();